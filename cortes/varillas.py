#!/usr/bin/env python


#############################################################################
##
## Copyright (C) 2013 Riverbank Computing Limited.
## Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
## All rights reserved.
##
## This file is part of the examples of PyQt.
##
## $QT_BEGIN_LICENSE:BSD$
## You may use this file under the terms of the BSD license as follows:
##
## "Redistribution and use in source and binary forms, with or without
## modification, are permitted provided that the following conditions are
## met:
##   * Redistributions of source code must retain the above copyright
##     notice, this list of conditions and the following disclaimer.
##   * Redistributions in binary form must reproduce the above copyright
##     notice, this list of conditions and the following disclaimer in
##     the documentation and/or other materials provided with the
##     distribution.
##   * Neither the name of Nokia Corporation and its Subsidiary(-ies) nor
##     the names of its contributors may be used to endorse or promote
##     products derived from this software without specific prior written
##     permission.
##
## THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
## "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
## LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
## A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
## OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
## SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
## LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
## DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
## THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
## (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
## OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
## $QT_END_LICENSE$
##
#############################################################################

from PyQt5.QtGui import (QIcon, QKeySequence)
import PyQt5.QtGui as QtGui
import io
import csv
import pandas as pd
import xlrd
import PyQt5.QtCore as QtCore
import PyQt5.QtWidgets as QtWidgets
from PyQt5.QtWidgets import (QAction, QApplication, QCheckBox, QComboBox,
        QDialog, QGridLayout, QGroupBox, QHBoxLayout, QLabel, QLineEdit,
        QMessageBox, QMenu, QPushButton, QSpinBox, QStyle, QSystemTrayIcon,
        QTextEdit, QVBoxLayout, QTableWidget, QTableWidgetItem)
import _curses_panel
import systray_rc


class Window(QDialog):
    def __init__(self):
        super(Window, self).__init__()

        self.createEntradaGroupBox()
        self.createDetalleFormaGroupBox()
        self.createSalidaFormaGroupBox()

        self.showMessageButton.clicked.connect(self.showMessage)
        self.loadExcelButton.clicked.connect(self.loadExcel)

        mainLayout = QVBoxLayout()
        mainLayout.addWidget(self.entradaGroupBox)
        mainLayout.addWidget(self.detalleFormasGroupBox)
        mainLayout.addWidget(self.salidaFormasGroupBox)
        self.setLayout(mainLayout)

        self.setWindowTitle("Cortes de Varillas")
        self.resize(900, 700)

    def setVisible(self, visible):
        super(Window, self).setVisible(visible)

    def closeEvent(self, event):
        QMessageBox.information(self, "Corte de Varillas",
                "Gracias por usar la aplicación")
        self.hide()
        #event.ignore()
        sys.exit(0)

    def loadExcel(self):
        documento = xlrd.open_workbook("/home/chrod/Escritorio/varillas.xlsx")
        sheet_names = documento.sheet_names()
        xl_sheet = documento.sheet_by_index(0)
        num_cols = xl_sheet.ncols  # Number of columns

        r=0
        for row_idx in range(0, xl_sheet.nrows):  # Iterate through rows
            #print(row_idx)
            if row_idx != 0:
                self.tableDetalleFormas.insertRow(r)
                #if row_idx > 0:
                for col_idx in range(0, num_cols):  # Iterate through columns
                    print(col_idx)
                    #if col_idx != 0:
                    cell_obj = xl_sheet.cell(row_idx, col_idx)  # Get cell object by row, col
                    newItem = QTableWidgetItem(str(cell_obj.value))
                    self.tableDetalleFormas.setItem(r, col_idx, newItem)

                r += 1

    def showMessage(self):
        print("Hola")
        newItem = QTableWidgetItem('Hola')
        self.tableDetalleFormas.setItem(0, 0, newItem)
        #icon = QSystemTrayIcon.MessageIcon(
        #        self.typeComboBox.itemData(self.typeComboBox.currentIndex()))
        #self.trayIcon.showMessage(self.titleEdit.text(),
        #        self.bodyEdit.toPlainText(), icon,
        #        self.durationSpinBox.value() * 1000)

    def createEntradaGroupBox(self):
        self.entradaGroupBox = QGroupBox("Entradas")

        varillasLabel = QLabel("Longitud de Varillas:")
        self.varillasSpinBox = QSpinBox()
        self.varillasSpinBox.setRange(9, 12)
        self.varillasSpinBox.setSuffix(" m")
        self.varillasSpinBox.setValue(9)

        nroFormasLabel = QLabel("# de Formas:")
        self.nroFormasSpinBox = QSpinBox()
        self.nroFormasSpinBox.setRange(1, 200)
        self.nroFormasSpinBox.setSuffix("")
        self.nroFormasSpinBox.setValue(30)

        self.showMessageButton = QPushButton("Generar Cortes")
        self.showMessageButton.setDefault(True)
        self.loadExcelButton = QPushButton("Cargar Excel")
        self.loadExcelButton.setDefault(True)

        messageLayout = QGridLayout()
        messageLayout.addWidget(varillasLabel, 0, 0)
        messageLayout.addWidget(self.varillasSpinBox, 0, 1, 1, 2)
        messageLayout.addWidget(nroFormasLabel, 1, 0)
        messageLayout.addWidget(self.nroFormasSpinBox, 1, 1, 1, 2)
        messageLayout.addWidget(self.showMessageButton, 2, 0)
        messageLayout.addWidget(self.loadExcelButton, 2, 1)
        messageLayout.setColumnStretch(3, 1)
        messageLayout.setRowStretch(4, 0)
        self.entradaGroupBox .setMaximumWidth(250)
        self.entradaGroupBox .setLayout(messageLayout)

    def createDetalleFormaGroupBox(self):
        self.detalleFormasGroupBox = QGroupBox("Detalle de Formas")

        self.tableDetalleFormas = QTableWidget(0, 4, self)
        self.tableDetalleFormas.setMinimumWidth(600)
        self.tableDetalleFormas.installEventFilter(self)
        self.tableDetalleFormas.setHorizontalHeaderLabels(('Code', 'Descripcion', 'Número', 'Longitud'))
        #cubesHeaderItem = QTableWidgetItem("Cubes")
        #self.tableDetalleFormas.setItem(0, 0, cubesHeaderItem)

        detalleLayout = QGridLayout()
        detalleLayout.addWidget(self.tableDetalleFormas, 0, 0)
        #detalleLayout.setColumnStretch(0, 0)
        #detalleLayout.setRowStretch(1, 0)
        self.detalleFormasGroupBox.setLayout(detalleLayout)
        self.detalleFormasGroupBox.setMaximumHeight(200)

    def createSalidaFormaGroupBox(self):
        self.salidaFormasGroupBox = QGroupBox("Detalle de Cortes Óptimos")

        self.tableSalidaFormas = QTableWidget(3, 4, self)
        self.tableSalidaFormas.setMinimumWidth(600)
        #self.tableDetalleFormas.setMaximumHeight(100)
        self.tableSalidaFormas.setHorizontalHeaderLabels(('Descripción Corte en 1 varilla', 'Long x 1 var', '%', '#Varillas'))
        #cubesHeaderItem = QTableWidgetItem("Cubes")
        #self.tableDetalleFormas.setItem(0, 0, cubesHeaderItem)

        detalleLayout = QGridLayout()
        detalleLayout.addWidget(self.tableSalidaFormas, 0, 0)
        #detalleLayout.setColumnStretch(0, 0)
        #detalleLayout.setRowStretch(1, 0)
        self.salidaFormasGroupBox .setLayout(detalleLayout)
        self.salidaFormasGroupBox.setMaximumHeight(300)

    def eventFilter(self, source, event):
        if (event.type() == QtCore.QEvent.KeyPress and
                event.matches(QtGui.QKeySequence.Copy)):
            self.copySelection()
            return True
        return super(Window, self).eventFilter(source, event)

    def copySelection(self):
        selection = self.tableView.selectedIndexes()
        if selection:
            rows = sorted(index.row() for index in selection)
            columns = sorted(index.column() for index in selection)
            rowcount = rows[-1] - rows[0] + 1
            colcount = columns[-1] - columns[0] + 1
            table = [[''] * colcount for _ in range(rowcount)]
            for index in selection:
                row = index.row() - rows[0]
                column = index.column() - columns[0]
                table[row][column] = index.data()
            stream = io.StringIO()
            csv.writer(stream).writerows(table)
            QtGui.qApp.clipboard().setText(stream.getvalue())

    def pasteSelection(self):
        selection = self.selectedIndexes()
        if selection:
            model = self.model()

            buffer = QtWidgets.qApp.clipboard().text()
            rows = sorted(index.row() for index in selection)
            columns = sorted(index.column() for index in selection)
            reader = csv.reader(io.StringIO(buffer), delimiter='\t')
            if len(rows) == 1 and len(columns) == 1:
                for i, line in enumerate(reader):
                    for j, cell in enumerate(line):
                        model.setData(model.index(rows[0]+i,columns[0]+j), cell)
            else:
                arr = [ [ cell for cell in row ] for row in reader]
                for index in selection:
                    row = index.row() - rows[0]
                    column = index.column() - columns[0]
                    model.setData(model.index(index.row(), index.column()), arr[row][column])
        return

if __name__ == '__main__':

    import sys

    app = QApplication(sys.argv)

    if not QSystemTrayIcon.isSystemTrayAvailable():
        QMessageBox.critical(None, "Corte de Varillas",
                "La ventana se ha ")
        sys.exit(1)

    QApplication.setQuitOnLastWindowClosed(False)

    window = Window()
    window.show()
    sys.exit(app.exec_())
